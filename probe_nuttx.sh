#!/bin/bash -e

## This script will create a copy of nuttx repositories that are
## saved at /opt/nuttx/ and will update it with the mainline.
## Then it will configure the selected board and compile it.
## When the compilation finishes the firmware will be flashed
## in the target board.

## Name of target board and its profile
source $1

## Verify if workspace exist

if [ ! -d ~/workspace ]; then
  mkdir ~/workspace
fi

## Verify if there is a nuttx repository

if [ -d ~/workspace/nuttx ]; then
  echo "Directory nuttx exist, removing it..."
  rm -rf ~/workspace/nuttx
fi

## Verify if there is an apps repository

if [ -d ~/workspace/apps ]; then
  echo "Directory apps exist, removing it..."
  rm -rf ~/workspace/apps
fi

echo "Cloning the NuttX repository..."
git clone /opt/nuttx/nuttx ~/workspace/nuttx
echo "Updating the nuttx repository"
cd ~/workspace/nuttx
git pull

echo "Cloning the Apps repository..."
git clone /opt/nuttx/apps ~/workspace/apps
echo "Updating the apps repository"
cd ~/workspace/apps
git pull

echo "Configuring board $BOARDNAME / $PROFILENAME ..."
cd ~/workspace/nuttx
./tools/configure.sh $BOARDNAME/$PROFILENAME

echo "Compiling it..."
make

echo "Verifying if firmware was created..."
if [ ! -f ~/workspace/nuttx/nuttx.bin ]; then
  echo "ERROR: File nuttx.bin was not created!"
  exit
fi

echo "Flashing the Firmware ..."
eval $FLASHCOMMAND
echo "Done"

