**RaspiberryPi Script to create a NuttX farm Quality Assurance **

This project is a low cost NuttX Farm Test solution.

All you will need is a Raspberry Pi with two USBs (for JTAG/SWD programmer and USB/Serial).

Initially we will use a STLink-V2 as programmer and FTDI FT232 or SiliconLabs CP2102 USB/Serial

---

## How to setup it

Clone this repository and copy it to RaspberryPi.

Also you will need to install the crosscompile tool in our raspi board.

---
